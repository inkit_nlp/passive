from nltk.corpus import gutenberg
from nltk.tokenize import sent_tokenize

def start():
	#sents = gutenberg.sents('austen-emma.txt')
	raw = gutenberg.raw('austen-emma.txt')
	#raw = raw.replace("\n"," ")
	sents = sent_tokenize(raw)

	for sent in sents:
		print sent.replace("\n"," ")
		

if __name__ == "__main__":
	start()