from collections import defaultdict

class Passive:
	def __init__(self):
		from parser import Parser
		self.parser = Parser()

	def isPassive(self, sentence):
		dependencies = self.parser.parseToStanfordDependencies(sentence)
		result_tags = [rel for rel, gov, dep in dependencies.dependencies]
		
		tupleResult = [(rel, gov.text, gov.start, gov.end, dep.text, dep.start, dep.end) for rel, gov, dep in dependencies.dependencies]
		#print '\n',tupleResult,'\n'

		passive_tags = ['auxpass','csubjpass','nsubjpass']
		#passive_tags = ['nsubjpass']
		common_tags = list(set(result_tags).intersection(passive_tags))
		

		parse1,parse_tree = self.parser.parse(sentence)
		parse_tree = self.removeScoreInfo(str(parse_tree))
		parse_tree = self.addInfoToParseTree(parse_tree,dependencies)
		aux_indices = self.findAuxiliarySubTree(parse_tree)
		# print tupleResult
		# print "\n\n"
		# print parse_tree
		# print "\n\n"
		# print aux_indices


		hasPassiveElement = False
		for result in tupleResult:

			if result[0] in passive_tags and [result[2],result[3]] not in aux_indices and [result[5],result[6]] not in aux_indices:
				hasPassiveElement = True

		return hasPassiveElement


	def findAuxiliarySubTree(self, parse_tree):
		aux_indices = []
		while parse_tree.find("(SBAR ")!=-1:
			parse_tree = parse_tree[parse_tree.find("(SBAR ")+5:].strip()
			open_brace, index = 1, 0
			while index<len(parse_tree):
				if parse_tree[index] == "(":
					open_brace += 1
				if parse_tree[index] == ")":
					open_brace -= 1
				if open_brace == 0:
					break
				index += 1
			aux_indices = aux_indices + self.findAuxIndicies(parse_tree[:index])
			parse_tree = parse_tree[index+1:]
		return aux_indices

	def findAuxIndicies(self,sbar_part):
		aux_indices = []
		sbar_part = sbar_part.split("::")
		for part in sbar_part:
			if part[0]=="[":
				part = part[1:part.find(']')]
				part = part.split(",")
				aux_indices.append([int(part[0]),int(part[1])])

		return aux_indices

	def removeScoreInfo(self, parse_tree):
		i = 0
		while i<len(parse_tree):
			if parse_tree[i] == '[':
				j = parse_tree.find(']',i)
				parse_tree = parse_tree[:i]+parse_tree[j+2:]
			else:
				i = i+1

		return parse_tree

	def addInfoToParseTree(self, parse_tree, dependencies):
		dependenciesResult = [[gov.text, gov.start, gov.end, dep.text, dep.start, dep.end] for rel, gov, dep in dependencies.dependencies]
		dependenciesMapping = defaultdict(list)
		for result in dependenciesResult:
			if [result[1],result[2]] not in dependenciesMapping[result[0]]:
				dependenciesMapping[result[0]].append([result[1],result[2]])

			if [result[4],result[5]] not in dependenciesMapping[result[3]]: 
				dependenciesMapping[result[3]].append([result[4],result[5]])


		parse_tree = parse_tree.split(")")

		for index,element in enumerate(parse_tree):
			if len(element)!=0:
				word = element[::-1]
				word = word[:word.find(' ')][::-1]
				if len(dependenciesMapping[word]) > 0:
					parse_tree[index] = element + "::" + str(dependenciesMapping[word].pop(0))
							
		return ")".join(parse_tree)			