from passive import Passive
import sys
import re

reload(sys)  
sys.setdefaultencoding('utf8')

def read_input(file_name):
	lines = open(file_name).readlines()
	sents,labels = [],[]
	
	for line in lines:
		line = line.split(":::")
		labels.append(line[0].strip())
		sents.append(line[1].strip())
	
	return sents,labels

def auto_label(cutoff):
	#sents = open(sys.argv[1]).readlines()
	fpassive = open("passive.log",'a')
	factive = open("active.log",'a')
	p = Passive()
	if cutoff == None:
		cutoff = -1
	count = 1
	with open(sys.argv[1]) as f:
		for sent in f:
			sent = sent.strip()
			#print "\n",count,"::",sent
			is_passive = p.isPassive(sent)
			if is_passive:
				print("P")
				fpassive.write(sent+"\n")
			else:
				print("A")
				factive.write(sent+"\n")
			
			if cutoff == 0:
				break
			cutoff -= 1
			count += 1
	fpassive.close()
	factive.close()

def test_label():
	sents,labels = read_input(sys.argv[1])
	ferror = open("error.log",'a')
	
	p = Passive()

	for i, sent in enumerate(sents):
		sent = sent.strip()
		print "\n",sent

		is_passive = p.isPassive(sent)
		
		if (is_passive == True and labels[i] == "P") or (is_passive == False and labels[i] == "A"):
			#print "\t\t","*"*20,"Passed","*"*20
			pass
		else:
			print "Failed:\t\tCorrect:",labels[i],"\t\t",sent
			ferror.write("Failed:\t\tCorrect:"+str(labels[i])+"\t\t"+(sent)+"\n")

		if cutoff == 0:
			break
		cutoff -= 1


if __name__ == "__main__":
	auto_label(-1)
	